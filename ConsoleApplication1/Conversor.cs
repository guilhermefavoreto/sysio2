﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using Jarloo.CardStock.Helpers;
using Jarloo.CardStock.Models;
using System.Collections.ObjectModel;
using System.Xml.Linq;


namespace ConsoleApplication1
{
    public class Conversor : Operacoes
    {
        private readonly DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Background);
        public ObservableCollection<Quote> Quotes { get; set; }
        public Conversor()
        {
            Quotes = new ObservableCollection<Quote>();

            //Some example tickers
            Quotes.Add(new Quote("BRLUSD=X"));

            //get the data
            YahooStockEngine.Fetch(Quotes);

            //poll every 25 seconds
            timer.Interval = new TimeSpan(0, 0, 25);
            timer.Tick += (o, e) => YahooStockEngine.Fetch(Quotes);

            timer.Start();

        }
        public void opera(string tipo, decimal valor, Dinheiro dollar, Dinheiro real, List<Dinheiro> lista)
        {
            var quote = Quotes.FirstOrDefault(c => c.Name == "BRL/USD");
            
            dollar.Cotacao = quote.LastTradePrice;

            if (tipo == "$")
            {
                dollar.Valor = dollar.Valor / dollar.Cotacao;
                var operacao = new Dinheiro()
                {
                    Cotacao = dollar.Cotacao,
                    Data = DateTime.Now,
                    Tipo = "$",
                    TipoOperacao = "Conversao(Dollar/Real)"
                };
                lista.Add(operacao);
             }
            else
                if (tipo == "R$")
            {
                real.Valor = real.Valor * dollar.Cotacao;
                var operacao = new Dinheiro()
                {
                    Cotacao = dollar.Cotacao,
                    Data = DateTime.Now,
                    Tipo = "R$",
                    TipoOperacao = "Conversao(Real/Dollar)"
                };
                lista.Add(operacao);
            }
        }
    }
}