﻿
using System.Collections.ObjectModel;
using Jarloo.CardStock.Models;
using System.Xml.Linq;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinheiro meuSaldoReal = new Dinheiro();
            Conversor meuConversor = new Conversor();
            Dinheiro meuSaldoDollar = new Dinheiro();

            var entrada = new Entrada();
            var saida = new Saida();
            var saldo = new Saldo();
            var conversor = new Conversor();
            var minhaLista = new List<Dinheiro>();
            var meuHistorico = new Historico();

            entrada.opera("$", 300.00M, meuSaldoReal, meuSaldoDollar, minhaLista);
            entrada.opera("R$", 300.00M, meuSaldoReal, meuSaldoDollar, minhaLista);
            conversor.opera("$", 300.00M, meuSaldoDollar, meuSaldoReal, minhaLista);
            conversor.opera("R$", 300.00M, meuSaldoDollar, meuSaldoReal, minhaLista);
            saida.opera("$", 100.00M, meuSaldoReal, meuSaldoDollar, minhaLista);
            meuHistorico.geraHistorico(minhaLista);

        }
    }
}