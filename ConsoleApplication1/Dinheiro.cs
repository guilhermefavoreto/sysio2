﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Dinheiro
    {
        public DateTime Data { get; set; }
        public String Tipo { get; set; }
        public decimal Cotacao { get; set; }
        public decimal Valor { get; set; }
        public String TipoOperacao { get; set; }
    }
}